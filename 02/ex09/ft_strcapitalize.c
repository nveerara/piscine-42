/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/30 21:35:02 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/02 23:06:58 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		is_lower(char c)
{
	return ('a' <= c && c <= 'z');
}

int		is_upper(char c)
{
	return ('A' <= c && c <= 'Z');
}

int		is_digit(char c)
{
	return (('0' <= c && c <= '9'));
}

int		is_alphanumeric(char c)
{
	return (is_digit(c) || is_upper(c) || is_lower(c));
}

char	*ft_strcapitalize(char *str)
{
	char *bstr;

	bstr = str;
	while (*str)
	{
		if (is_alphanumeric(*str))
		{
			if (is_lower(*str))
				*str -= 0x20;
			str++;
			while (is_alphanumeric(*str))
			{
				if (is_upper(*str))
					*str += 0x20;
				str++;
			}
		}
		else
			str++;
	}
	return (bstr);
}
