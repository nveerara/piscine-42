/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_non_printable.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/01 10:12:22 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/01 20:31:24 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putnbr_rec_hex_fixed_size(unsigned char nb, int size)
{
	char	digit;

	if (nb != 0 || size)
	{
		if (nb % 0x10 < 0xA)
			digit = '0' + nb % 0x10;
		else
			digit = 'a' + nb % 0x10 - 0xA;
		ft_putnbr_rec_hex_fixed_size(nb / 0x10, size - 1);
		write(1, &digit, 1);
	}
}

void	ft_putstr_non_printable(char *str)
{
	while (*str)
	{
		if (0x20 <= *str && *str <= 0x7E)
			write(1, str, 1);
		else
		{
			write(1, "\\", 1);
			ft_putnbr_rec_hex_fixed_size((unsigned char)*str, 2);
		}
		str++;
	}
}
