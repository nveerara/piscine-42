/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_memory.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/01 11:53:48 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/01 20:28:03 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include <unistd.h>

static void	ft_putnbr_rec_hex_fixed_size(unsigned int nb, unsigned int size)
{
	char	digit;

	if (nb != 0 || size)
	{
		if (nb % 0x10 < 0xA)
			digit = '0' + (nb & 0b1111);
		else
			digit = 'a' + (nb & 0b1111) - 0xA;
		ft_putnbr_rec_hex_fixed_size(nb >> 4, size - 1);
		write(1, &digit, 1);
	}
}

static void	printmemaddr(void *addr)
{
	ft_putnbr_rec_hex_fixed_size((size_t)addr, 16);
	write(1, ": ", 2);
}

static void	printhexchar(void *addr, unsigned int size)
{
	int i;
	int offset;
	int j;

	i = 0;
	while (size && i < 16)
	{
		offset = 0b11111111;
		j = 0;
		while (j < 4 && size)
		{
			ft_putnbr_rec_hex_fixed_size((*(unsigned int *)(addr + i) & (offset << (j * 8))) >> (j * 8) , 2);
			size--;
			j++;
			if (j % 2 == 0)
				write(1, " ", 1);
		}
		i += j;
	}
	if (size == 0 && i < 16)
	{
		write(1, "    ", 4);
	}
	write(1, " ", 1);
}

static void	printchar(void *addr, unsigned int size)
{
	int i;
	int offset;
	int j;

	i = 0;
	while (size && i < 16)
	{
		offset = 0b11111111;
		j = 0;
		while (j < 4 && size)
		{
			if (0x20 <= ((*(int *)(addr + i) & (offset << (j * 8))) >> (j * 8))
					&& ((*(int *)(addr + i) & (offset << (j * 8))) >> (j * 8)) <= 0x7E)
			{
				if (!(i != 0 &&
			((*(int *)(addr + i) & (offset << (j * 8))) >> (j * 8))
			== ((*(int *)(addr + i - 0) & (offset << (j * 8))) >> (j * 8))))
					write(1, (addr + i), 1);
			}
			else
				write(1, ".", 1);
			size--;
			j++;
		}
		i += j;
	}
	write(1, "\n", 1);
}

void	*ft_print_memory(void *addr, unsigned int size)
{
	unsigned int		offset;

	offset = 0;
	while (offset < size)
	{
		printmemaddr(addr + offset);
		printhexchar(addr + offset, size - offset);
		printchar(addr + offset, size - offset);
		offset += 0x10;
	}
	return (addr);
}
