/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/02 12:19:14 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/05 12:18:44 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		ft_strlen(char *str)
{
	int	cc;

	cc = 0;
	while (*(str + cc))
		cc++;
	return (cc);
}

void	ft_putnbr_rec_base(int nb, char *base, int baselen)
{
	char	digit;

	if (nb != 0)
	{
		digit = base[(0 <= nb ? nb % baselen : nb % baselen * -1)];
		ft_putnbr_rec_base(nb / baselen, base, baselen);
		write(1, &digit, 1);
	}
}

int		check_base(char *base)
{
	int i;
	int j;

	i = 0;
	while (*(base + i))
		i++;
	if (i == 0 || i == 1)
		return (1);
	i = 0;
	while (*(base + i))
	{
		j = 1;
		if (*(base + i) == '-' || *(base + i) == '+')
			return (1);
		while (*(base + j + i))
		{
			if (*(base + i + j) == *(base + i))
				return (1);
			j++;
		}
		i++;
	}
	return (0);
}

void	ft_putnbr_base(int nb, char *base)
{
	if (!check_base(base))
	{
		if (nb == 0)
			write(1, base, 1);
		else
		{
			if (nb < 0)
				write(1, "-", 1);
			ft_putnbr_rec_base(nb, base, ft_strlen(base));
		}
	}
}
