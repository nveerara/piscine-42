/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/04 10:32:29 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/05 14:04:35 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	rush00(int x, int y);
void	rush01(int x, int y);
void	rush02(int x, int y);
void	rush03(int x, int y);
void	rush04(int x, int y);

int		ft_atoi(char *str)
{
	int		neg;
	int		atoi;

	neg = 1;
	atoi = 0;
	while (*str == ' ' || *str == '\r' || *str == '\f'
			|| *str == '\v' || *str == '\n' || *str == '\t')
		str++;
	while (*str == '-' || *str == '+')
	{
		if (*str == '-')
			neg *= -1;
		str++;
	}
	while ('0' <= *str && *str <= '9')
	{
		atoi *= 10;
		atoi += *str - 0x30;
		str++;
	}
	atoi *= neg;
	return (atoi);
}

void	(*g_print_rect[])(int, int) = {&rush00
	, &rush01
	, &rush02
	, &rush03
	, &rush04
	, NULL};

int		main(int ac, char **av)
{
	int rush;
	int x;
	int y;

	if  (1 < ac)
	{
		rush = ft_atoi(av[1]);
		if (0 <= rush && rush <= 4)
		{
			if (ac == 4)
			{
				x = ft_atoi(av[2]);
				y = ft_atoi(av[3]);
				if (0 <= rush && rush <= 4)
					g_print_rect[rush](x, y);
			}
			else
				g_print_rect[rush](5, 5);
		}
	}
	else
		g_print_rect[0](5, 5);
}
