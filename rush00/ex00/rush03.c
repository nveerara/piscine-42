/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush03.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/04 16:10:13 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/05 14:00:42 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#define TOP_LEFT_CHAR 'A'
#define BOTTOM_LEFT_CHAR 'A'
#define TOP_RIGHT_CHAR 'C'
#define BOTTOM_RIGHT_CHAR 'C'
#define TOP_WALL_CHAR 'B'
#define BOTTOM_WALL_CHAR 'B'
#define LEFT_WALL_CHAR 'B'
#define RIGHT_WALL_CHAR 'B'
#define EMPTY_SPACE_CHAR ' '

#define X 0
#define Y 1

void		ft_putchar(char c);

static char	check_pos(int *pos, int x, int y)
{
	if (pos[X] == 0 && pos[Y] == 0)
		return (TOP_LEFT_CHAR);
	else if (pos[X] == 0 && pos[Y] == y)
		return (BOTTOM_LEFT_CHAR);
	else if (pos[X] == x && pos[Y] == 0)
		return (TOP_RIGHT_CHAR);
	else if (pos[X] == x && pos[Y] == y)
		return (BOTTOM_RIGHT_CHAR);
	else if (pos[Y] == 0)
		return (TOP_WALL_CHAR);
	else if (pos[Y] == y)
		return (BOTTOM_WALL_CHAR);
	else if (pos[X] == 0)
		return (LEFT_WALL_CHAR);
	else if (pos[X] == x)
		return (RIGHT_WALL_CHAR);
	return (EMPTY_SPACE_CHAR);
}

void		rush03(int x, int y)
{
	int pos[2];

	x--;
	y--;
	pos[Y] = 0;
	while (pos[Y] <= y)
	{
		pos[X] = 0;
		while (pos[X] <= x)
		{
			ft_putchar(check_pos(pos, x, y));
			pos[X]++;
		}
		if (0 <= x)
			ft_putchar('\n');
		pos[Y]++;
	}
}
