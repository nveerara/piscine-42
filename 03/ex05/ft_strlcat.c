/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/02 10:00:26 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/03 14:23:53 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

unsigned int	ft_strlcat(char *dest, char *src, unsigned int size)
{
	unsigned int	destsize;
	unsigned int	srcsize;

	destsize = 0;
	srcsize = 0;
	while (*dest && destsize < size)
	{
		dest++;
		destsize++;
	}
	if (*dest == '\0' && destsize + 1 < size)
	{
		while (destsize + 1 < size && *src)
		{
			*(dest++) = *(src++);
			destsize++;
		}
		*dest = '\0';
		size--;
	}
	while (*(src++))
		srcsize++;
	return (destsize + srcsize);
}
