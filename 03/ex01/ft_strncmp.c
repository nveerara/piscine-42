/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/01 16:06:47 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/02 19:48:31 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_strncmp(char *s1, char *s2, unsigned int n)
{
	if (n)
	{
		while (*s1 == *s2 && *s1 && n)
		{
			s1++;
			s2++;
			n--;
		}
		if (n)
			return (*s1 - *s2);
	}
	return (0);
}
