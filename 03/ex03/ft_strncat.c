/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/01 18:24:16 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/01 18:28:00 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strncat(char *dest, char *src, unsigned int nb)
{
	char	*bdest;

	bdest = dest;
	while (*dest)
		dest++;
	while (*src && nb)
	{
		*(dest++) = *(src++);
		nb--;
	}
	*dest = '\0';
	return (bdest);
}
