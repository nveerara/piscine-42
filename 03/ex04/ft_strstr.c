/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/01 21:47:28 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/02 20:29:19 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char	*ft_strstr(char *str, char *to_find)
{
	int		i;

	if (*str == '\0' && *to_find == '\0')
		return (str);
	while (*str)
	{
		i = 0;
		while (*(to_find + i) == *(str + i) && *(to_find + i) && *(str + i))
			i++;
		if (*(to_find + i) == '\0')
			return (str);
		str++;
	}
	return (NULL);
}
