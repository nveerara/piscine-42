/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_file.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/07 04:13:10 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/10 10:38:43 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DISPLAY_FILE_H
# define DISPLAY_FILE_H

# include <fcntl.h>
# include <unistd.h>
# include <string.h>
# include <stdio.h>
# include <sys/errno.h>
# include <libgen.h>

# define BUFFER_SIZE 64

# define FILE_MISSING "File name missing.\n"
# define TOO_MANY_ARG "Too many arguments.\n"
# define CANNOT_OPEN_FILE "Cannot read file\n"

void	readfile(int ac, char **av);
void	ft_putstr(char *str);
void	ft_putstr_fd(char *str, int fd);
void	readstdin(void);

#endif
