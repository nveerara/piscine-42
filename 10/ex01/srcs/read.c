/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/07 04:17:59 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/10 10:39:27 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "display_file.h"

void	puterror(char **av, int n)
{
	ft_putstr_fd(basename(av[0]), STDERR_FILENO);
	ft_putstr_fd(": ", STDERR_FILENO);
	ft_putstr_fd(av[n], STDERR_FILENO);
	ft_putstr_fd(": ", STDERR_FILENO);
	ft_putstr_fd(strerror(errno), STDERR_FILENO);
	ft_putstr_fd("\n", STDERR_FILENO);
}

void	readfile(int ac, char **av)
{
	char	buf[BUFFER_SIZE];
	int		ret;
	int		fd;
	int		n;

	n = 1;
	while (n < ac)
	{
		if ((fd = open(av[n], O_RDONLY)) == -1)
			puterror(av, n);
		else
		{
			while ((ret = read(fd, buf, BUFFER_SIZE)))
				write(1, buf, ret);
			if (close(fd) == -1)
				puterror(av, n);
		}
		n++;
	}
}

void	readstdin(void)
{
	char	buf[BUFFER_SIZE];
	int		ret;

	while ((ret = read(STDIN_FILENO, buf, BUFFER_SIZE)))
		write(1, buf, ret);
}
