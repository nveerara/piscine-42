/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_file.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/07 04:13:10 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/10 10:41:36 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DISPLAY_FILE_H
# define DISPLAY_FILE_H

# include <fcntl.h>
# include <unistd.h>
# include <string.h>
# include <stdio.h>
# include <sys/errno.h>
# include <stdlib.h>
# include <libgen.h>

# define BUFFER_SIZE 55

# define FLAGS "C"

typedef struct	s_flags {
	void		(*lecture_mode)(char *, int, int);
	int			print_success;
}				t_flags;

extern t_flags	g_flags;

void			puterror(char **av, int n);
void			check_flags(int ac, char **av, int *n);
void			reads(int ac, char **av, int *n);
int				check(char *str);
void			err(int ac, char **av, int *n);
void			flag_c(int ac, char **av, int *n);
void			read_mode_c(int ac, char **av, int *n);
void			read_mode_n(int ac, char **av, int *n);
void			read_error(char *buf, int last, int ret);
void			print_line_n(char *buf, int last, int ret);
void			print_line_c(char *buf, int last, int ret);
void			puterror(char **av, int n);

#endif
