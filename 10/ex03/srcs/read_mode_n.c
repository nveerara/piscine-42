/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_mode_n.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/07 09:22:55 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/09 17:56:52 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "display_file.h"

static void	print_addr(int len, int dupe)
{
	static int		addr = 0;

	if (!dupe)
		ft_putnbr_base_pad(addr, "0123456789abcdef", 7);
	addr += len;
}

static void	print_hex(char *buffer, int len)
{
	int		n;

	n = 0;
	print_addr(len, 0);
	while (n < 16)
	{
		ft_putstr(" ");
		if (n < len)
			ft_putnbr_base_pad((unsigned char)buffer[n], "0123456789abcdef", 2);
		else
			ft_putstr("  ");
		n++;
	}
	ft_putstr("\n");
}

static void	print_line_buffer(char **buffer)
{
	static char		prevline[17] = "";
	static int		duplicate = 0;
	int				n;

	n = 0;
	while (16 <= ft_strlen(buffer[1]) - n)
	{
		if (ft_strncmp(prevline, buffer[1] + n, 16) == 0)
		{
			if (!duplicate)
				write(1, "*\n", 2);
			duplicate = 1;
			print_addr(0x10, 1);
		}
		else
		{
			duplicate = 0;
			print_hex(buffer[1] + n, 16);
		}
		ft_strncpy(prevline, buffer[1] + n, 16);
		n += 16;
	}
	if (buffer[0] != NULL)
		free(buffer[0]);
	buffer[0] = ft_strdup(buffer[1] + n);
}

void		print_line_n(char *buf, int last, int ret)
{
	static char		*buffer[2] = {NULL, NULL};
	char			*strj[2];

	buf[ret] = '\0';
	if (last == 0)
	{
		strj[0] = buffer[0];
		strj[1] = buf;
		if ((buffer[1] = ft_strjoin(2, strj, "")) == NULL)
			return ;
		print_line_buffer(buffer);
		free(buffer[1]);
	}
	else
	{
		if (buffer[0] != NULL)
		{
			print_hex(buffer[0], ft_strlen(buffer[0]));
			print_addr(ft_strlen(buffer[0]), 0);
			buffer[0] = NULL;
			ft_putstr("\n");
		}
	}
}
