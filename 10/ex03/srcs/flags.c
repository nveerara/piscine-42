/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flags.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/07 07:02:27 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/09 20:48:09 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "display_file.h"

void	err(int ac, char **av, int *n)
{
	n++;
	ac++;
	g_flags.lecture_mode = &read_error;
	ft_putstr_fd(av[0], STDERR_FILENO);
	ft_putstr_fd(" illegal option -- ", STDERR_FILENO);
}

t_flags g_flags = {&print_line_n, 0};

void	flag_c(int ac, char **av, int *n)
{
	ac++;
	av++;
	n++;
	g_flags.lecture_mode = &print_line_c;
}
