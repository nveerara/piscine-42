/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/07 04:05:47 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/09 20:49:08 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "display_file.h"

int	main(int ac, char **av)
{
	int		n;

	n = 1;
	if (1 <= ac)
	{
		check_flags(ac, av, &n);
		reads(ac, av, &n);
		if (errno && g_flags.print_success == 0)
			puterror(av, n - 1);
	}
}
