/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_error.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/08 19:26:59 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/10 10:42:02 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "display_file.h"

void	puterror(char **av, int n)
{
	ft_putstr_fd(basename(av[0]), STDERR_FILENO);
	ft_putstr_fd(": ", STDERR_FILENO);
	ft_putstr_fd(av[n], STDERR_FILENO);
	ft_putstr_fd(": ", STDERR_FILENO);
	ft_putstr_fd(strerror(errno), STDERR_FILENO);
	ft_putstr_fd("\n", STDERR_FILENO);
}

void	print_filename(char *fn)
{
	ft_putstr("==> ");
	ft_putstr(fn);
	ft_putstr(" <==\n");
}

void	read_error(char *buf, int last, int ret)
{
	buf++;
	last++;
	ret++;
}
