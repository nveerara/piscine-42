/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checkflags.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/07 05:49:08 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/08 22:21:12 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "display_file.h"

void	(*g_setflag[])(int, char **, int *) = {&flag_c, &err};

int		check(char *c)
{
	char	*flags;
	int		n;
	int		m;

	flags = FLAGS;
	n = 0;
	while (flags[n])
	{
		m = 0;
		while (c[m])
		{
			if (c[m] == flags[n])
				return (n);
			m++;
		}
		n++;
	}
	return (n);
}

void	check_flags(int ac, char **av, int *n)
{
	while (*n < ac && av[*n][0] == '-')
	{
		g_setflag[check(av[*n])](ac, av, n);
		(*n)++;
	}
}
