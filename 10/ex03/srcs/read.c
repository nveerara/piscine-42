/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/07 04:17:59 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/09 20:48:31 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "display_file.h"

void		read_stdin(void)
{
	int		ret;
	char	buf[BUFFER_SIZE + 1];

	while ((ret = read(STDIN_FILENO, buf, BUFFER_SIZE)))
	{
		buf[ret] = '\0';
		(*g_flags.lecture_mode)(buf, 0, ret);
	}
	(*g_flags.lecture_mode)(buf, 1, ret);
}

void		read_files(int ac, char **av, int *n)
{
	int		fd;
	int		ret;
	char	buf[BUFFER_SIZE + 1];

	ret = 0;
	while (*n < ac)
	{
		if ((fd = open(av[*n], O_RDONLY)) == -1)
			puterror(av, *n);
		else
		{
			g_flags.print_success = 1;
			while ((ret = read(fd, buf, BUFFER_SIZE)))
				(*g_flags.lecture_mode)(buf, 0, ret);
		}
		close(fd);
		(*n)++;
	}
	(*g_flags.lecture_mode)(buf, 1, ret);
}

void		reads(int ac, char **av, int *n)
{
	if (*n < ac)
		read_files(ac, av, n);
	else
		read_stdin();
}
