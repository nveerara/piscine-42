/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/07 04:05:47 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/09 17:44:29 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "display_file.h"

int	main(int ac, char **av)
{
	int	fd;

	if (ac <= 1)
		ft_putstr_fd(FILE_MISSING, STDERR_FILENO);
	else if (ac > 2)
		ft_putstr_fd(TOO_MANY_ARG, STDERR_FILENO);
	else
	{
		fd = open(av[1], O_RDONLY);
		if (fd == -1)
			ft_putstr_fd(CANNOT_OPEN_FILE, STDERR_FILENO);
		else
		{
			readfile(fd);
			close(fd);
		}
	}
}
