/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/07 04:17:59 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/08 23:09:15 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "display_file.h"

void	readfile(int fd)
{
	char	buf[BUFFER_SIZE];
	int		ret;

	while ((ret = read(fd, buf, BUFFER_SIZE)))
		write(1, buf, ret);
}
