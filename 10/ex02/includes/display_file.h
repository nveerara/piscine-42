/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_file.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/07 04:13:10 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/10 10:41:11 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DISPLAY_FILE_H
# define DISPLAY_FILE_H

# include <fcntl.h>
# include <unistd.h>
# include <string.h>
# include <stdio.h>
# include <sys/errno.h>
# include <stdlib.h>
# include <libgen.h>

# define BUFFER_SIZE 64

# define FLAGS "nc"

typedef struct	s_flags {
	int			multi_files;
	void		(*lecture_mode)(char *, int, int);
	int			flag_n_value;
	int			flag_c_value;
}				t_flags;

extern t_flags	g_flags;

void			reads(int ac, char **av, int *n);
int				check_flags(int ac, char **av, int *n);
void			read_files(int ac, char **av, int *n);
int				check(char *str);
int				err(int ac, char **av, int *n);
int				flag_c(int ac, char **av, int *n);
int				flag_n(int ac, char **av, int *n);
void			print_option_req_arg(char *av);
void			print_illegal_offset(char *offset);
void			print_usage(void);
void			print_line_c(char *buf, int ret, int last);
void			print_line_n(char *buf, int ret, int last);

#endif
