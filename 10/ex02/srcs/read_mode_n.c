/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_mode_n.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/07 09:22:55 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/09 17:12:58 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "display_file.h"
#include "libft.h"

void		print_last_n_lines(char *buffer, int len)
{
	int n;
	int	lc;

	n = len;
	lc = 0;
	while (n && lc <= g_flags.flag_n_value)
	{
		if (buffer[n - 1] == '\n')
			lc++;
		n--;
	}
	write(1, buffer + n + 1, len - n - 1);
}

void		print_line_n(char *buf, int ret, int last)
{
	static char		*buffer[2] = {NULL, NULL};
	char			*strj[2];
	static int		len = 0;

	buf[ret] = '\0';
	if (last == 0)
	{
		strj[0] = buffer[0];
		strj[1] = buf;
		if ((buffer[1] = ft_strjoin(2, strj, "")) == NULL)
			return ;
		free(buffer[0]);
		buffer[0] = buffer[1];
		len += ret;
	}
	else
	{
		if (buffer[0] != NULL)
		{
			print_last_n_lines(buffer[0], len);
			free(buffer[0]);
			buffer[0] = NULL;
			len = 0;
		}
	}
}
