/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/07 04:17:59 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/10 10:40:52 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "display_file.h"
#include "libft.h"

void		puterror(char **av, int n)
{
	ft_putstr_fd(basename(av[0]), STDERR_FILENO);
	ft_putstr_fd(": ", STDERR_FILENO);
	ft_putstr_fd(av[n], STDERR_FILENO);
	ft_putstr_fd(": ", STDERR_FILENO);
	ft_putstr_fd(strerror(errno), STDERR_FILENO);
	ft_putstr_fd("\n", STDERR_FILENO);
}

void		print_filename(char *fn)
{
	ft_putstr("==> ");
	ft_putstr(fn);
	ft_putstr(" <==\n");
}

void		read_stdin(void)
{
	int		ret;
	char	buf[BUFFER_SIZE + 1];

	while ((ret = read(STDIN_FILENO, buf, BUFFER_SIZE)))
		(*g_flags.lecture_mode)(buf, ret, 0);
	(*g_flags.lecture_mode)(buf, ret, 1);
}

void		read_files(int ac, char **av, int *n)
{
	int			fd;
	int			ret;
	char		buf[BUFFER_SIZE + 1];
	static int	first = 0;

	ret = 0;
	while (*n < ac)
	{
		if ((fd = open(av[*n], O_RDONLY)) == -1)
			puterror(av, *n);
		else
		{
			if (first)
				ft_putstr("\n");
			if (g_flags.multi_files)
				print_filename(av[*n]);
			while ((ret = read(fd, buf, BUFFER_SIZE)))
				(*g_flags.lecture_mode)(buf, ret, 0);
			(*g_flags.lecture_mode)(buf, ret, 1);
			if (close(fd) == -1)
				puterror(av, *n);
			first++;
		}
		(*n)++;
	}
}

void		reads(int ac, char **av, int *n)
{
	if (*n < ac)
		read_files(ac, av, n);
	else
		read_stdin();
}
