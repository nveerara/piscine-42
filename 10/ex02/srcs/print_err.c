/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_err.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/09 10:12:07 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/09 19:40:52 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "display_file.h"

void	print_usage(void)
{
	ft_putstr_fd("usage: tail [-F | -f | -r] [-q] ", STDERR_FILENO);
	ft_putstr_fd("[-b # | -c # | -n #] [file ...]\n", STDERR_FILENO);
}

void	print_illegal_offset(char *offset)
{
	ft_putstr_fd("tail: illegal offset -- ", STDERR_FILENO);
	ft_putstr_fd(offset, STDERR_FILENO);
	ft_putstr_fd("\n", STDERR_FILENO);
}

void	print_option_req_arg(char *av)
{
	ft_putstr_fd("tail: option requires an argument -- ", STDERR_FILENO);
	ft_putstr_fd(av, STDERR_FILENO);
	ft_putstr_fd("\n", STDERR_FILENO);
}
