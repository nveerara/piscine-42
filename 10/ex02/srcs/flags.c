/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flags.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/07 07:02:27 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/09 17:09:12 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "display_file.h"
#include "libft.h"

int		err(int ac, char **av, int *n)
{
	av++;
	n++;
	ac++;
	return (1);
}

t_flags g_flags = {0, &print_line_n, 10, 512};

int		flag_c_check(int ac, char **av, int *n)
{
	int i;

	i = 0;
	while (av[*n][i])
	{
		if (!('0' <= av[*n][i] && av[*n][i] <= '9'))
		{
			print_illegal_offset(av[*n]);
			i++;
			while (*n < ac)
				(*n)++;
			return (1);
		}
		i++;
	}
	return (0);
}

int		flag_c(int ac, char **av, int *n)
{
	int	i;

	i = 0;
	g_flags.lecture_mode = &print_line_c;
	(*n)++;
	if (*n == ac)
	{
		print_option_req_arg(av[*n - 1] + 1);
		print_usage();
		return (1);
	}
	else
	{
		g_flags.flag_c_value = ft_atoi(av[*n]);
		if (flag_c_check(ac, av, n))
			return (1);
	}
	return (0);
}

int		flag_n(int ac, char **av, int *n)
{
	int	i;

	i = 0;
	g_flags.lecture_mode = &print_line_n;
	(*n)++;
	if (*n == ac)
	{
		print_option_req_arg(av[*n - 1] + 1);
		print_usage();
		return (1);
	}
	else
	{
		g_flags.flag_n_value = ft_atoi(av[*n]);
		if (flag_c_check(ac, av, n))
			return (1);
	}
	return (0);
}
