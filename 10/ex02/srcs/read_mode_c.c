/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_mode_c.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/07 08:47:27 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/09 17:11:22 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "display_file.h"
#include "libft.h"

void		print_line_c(char *buf, int ret, int last)
{
	static char		*buffer[2] = {NULL, NULL};
	char			*strj[2];
	static int		len = 0;

	buf[ret] = '\0';
	if (last == 0)
	{
		strj[0] = buffer[0];
		strj[1] = buf;
		if ((buffer[1] = ft_strjoin(2, strj, "")) == NULL)
			return ;
		free(buffer[0]);
		buffer[0] = buffer[1];
		buffer[1] = NULL;
		len += ret;
	}
	else
	{
		write(1,
	buffer[0] + (len < g_flags.flag_c_value ? 0 : len - g_flags.flag_c_value),
				(len < g_flags.flag_c_value ? len : g_flags.flag_c_value));
		free(buffer[0]);
		buffer[0] = NULL;
		len = 0;
	}
}
