/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   btree_apply_by_level.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/11 23:25:42 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/13 09:06:19 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_btree.h"

int		btree_apply_at_level(t_btree **tab_node[2], int level
		, void (*applyf)(void *item, int current_level, int is_first_elem))
{
	int		i;
	int		j;
	int		first_e;

	i = 0;
	j = 0;
	first_e = 1;
	while (tab_node[0][i] != NULL)
	{
		(*applyf)(tab_node[0][i]->item, level, first_e);
		first_e = 0;
		if (tab_node[0][i]->left != NULL)
			tab_node[1][j++] = tab_node[0][i]->left;
		if (tab_node[0][i]->right != NULL)
			tab_node[1][j++] = tab_node[0][i]->right;
		i++;
	}
	tab_node[1][j] = NULL;
	free(tab_node[0]);
	tab_node[0] = tab_node[1];
	return (j * 2);
}

void	btree_apply_by_level(t_btree *root
		, void (*applyf)(void *item, int current_level, int is_first_elem))
{
	t_btree	**tab_node[2];
	int		level;
	int		nextsize;

	if (root != NULL)
	{
		level = 0;
		if ((tab_node[0] = (t_btree **)malloc(sizeof(t_btree *) * 2)) == NULL)
			return ;
		nextsize = 2;
		tab_node[0][0] = root;
		tab_node[0][1] = NULL;
		while (tab_node[0][0] != NULL)
		{
			if ((tab_node[1] = (t_btree **)malloc(sizeof(t_btree *)
							* (nextsize + 1))) == NULL)
				return ;
			nextsize = btree_apply_at_level(tab_node, level, applyf);
			level++;
		}
		free(tab_node[1]);
	}
}
