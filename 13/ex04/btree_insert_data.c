/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   btree_insert_data.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/11 22:46:22 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/12 23:13:22 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_btree.h"

void	btree_insert_data(t_btree **root, void *item
		, int (*cmpf)(void *, void *))
{
	t_btree	*branch[2];

	if ((branch[0] = *root) == NULL)
		*root = btree_create_node(item);
	else
	{
		branch[1] = *root;
		while (branch[1] != NULL)
		{
			if (0 <= (*cmpf)(item, branch[0]->item))
			{
				if ((branch[1] = branch[0]->right) == NULL)
					branch[0]->right = btree_create_node(item);
				else
					branch[0] = branch[0]->right;
			}
			else
			{
				if ((branch[1] = branch[0]->left) == NULL)
					branch[0]->left = btree_create_node(item);
				else
					branch[0] = branch[0]->left;
			}
		}
	}
}
