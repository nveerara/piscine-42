/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   btree_search_item.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/11 22:58:27 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/13 09:15:53 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_btree.h"

void	*btree_search_item(t_btree *root, void *data_ref
		, int (*cmpf)(void *, void *))
{
	t_btree	*to_find;

	to_find = NULL;
	if (root->left != NULL)
		to_find = btree_search_item(root->left, data_ref, cmpf);
	if (to_find == NULL && (*cmpf)(data_ref, root->item) == 0)
		to_find = root->item;
	if (to_find == NULL && root->right != NULL)
		to_find = btree_search_item(root->right, data_ref, cmpf);
	return (to_find);
}
