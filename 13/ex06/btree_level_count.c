/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   btree_level_count.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/11 23:11:25 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/13 09:04:01 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_btree.h"

int	btree_level_count(t_btree *root)
{
	int leftc;
	int rightc;

	leftc = 1;
	rightc = 1;
	if (root->left != NULL)
		leftc += btree_level_count(root->left);
	if (root->right != NULL)
		rightc += btree_level_count(root->right);
	return (leftc < rightc ? rightc : leftc);
}
