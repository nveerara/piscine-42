/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_push_back.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/09 13:22:00 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/09 18:33:31 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"
#include <stdlib.h>

void		ft_list_push_back(t_list **begin_list, void *data)
{
	t_list	*blist;

	blist = *begin_list;
	if (blist != NULL)
	{
		while (blist->next != NULL)
			blist = blist->next;
		blist->next = ft_create_elem(data);
	}
	else
		*begin_list = ft_create_elem(data);
}
