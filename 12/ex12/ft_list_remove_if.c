/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_remove_if.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/09 21:07:09 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/10 15:02:32 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"
#include <stdlib.h>

void	ft_list_remove_if(t_list **begin_list,
		void *data_ref, int (*cmp)(), void (*free_fct)(void *))
{
	t_list	*tmp;
	t_list	*clist;
	t_list	*plist;

	clist = *begin_list;
	while (clist != NULL)
	{
		if ((*cmp)(clist->data, data_ref) == 0)
		{
			tmp = clist->next;
			if (clist == *begin_list)
				*begin_list = (*begin_list)->next;
			else
				plist->next = tmp;
			(*free_fct)(clist->data);
			free(clist);
			clist = tmp;
		}
		else
		{
			plist = clist;
			clist = clist->next;
		}
	}
}
