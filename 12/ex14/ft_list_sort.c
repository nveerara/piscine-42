/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_sort.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/09 21:23:53 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/09 23:27:30 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"
#include <stdlib.h>

void	ft_list_sort(t_list **begin_list, int (*cmp)())
{
	t_list	*clist;
	t_list	*jlist;
	void	*tmp;

	clist = *begin_list;
	if (clist != NULL)
	{
		while (clist->next != NULL)
		{
			jlist = clist->next;
			while (jlist != NULL)
			{
				if ((*cmp)(clist->data, jlist->data) > 0)
				{
					tmp = clist->data;
					clist->data = jlist->data;
					jlist->data = tmp;
				}
				jlist = jlist->next;
			}
			clist = clist->next;
		}
	}
}
