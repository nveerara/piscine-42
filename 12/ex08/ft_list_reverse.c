/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_reverse.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/09 18:53:50 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/10 12:11:57 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"
#include <stdlib.h>

void	ft_list_reverse(t_list **begin_list)
{
	t_list	*tmp;
	t_list	*clist;

	if (*begin_list != NULL)
	{
		clist = (*begin_list)->next;
		(*begin_list)->next = NULL;
		while (clist != NULL)
		{
			tmp = clist->next;
			clist->next = *begin_list;
			*begin_list = clist;
			clist = tmp;
		}
	}
}
