/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_merge.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/09 21:18:44 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/10 15:10:17 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"
#include <stdlib.h>

void	ft_list_merge(t_list **begin_list1, t_list *begin_list2)
{
	t_list	*list1;

	if (*begin_list1 == NULL)
		*begin_list1 = begin_list2;
	else
	{
		list1 = *begin_list1;
		while (list1->next != NULL)
			list1 = list1->next;
		list1->next = begin_list2;
	}
}
