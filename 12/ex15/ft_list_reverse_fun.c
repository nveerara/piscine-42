/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_reverse_fun.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/09 21:35:04 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/10 10:27:50 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"
#include <stdlib.h>

void	ft_list_reverse_fun(t_list *begin_list)
{
	int		limit[2];
	int		n;
	t_list	*jlist;
	void	*tmp;

	limit[0] = 0;
	limit[1] = 0;
	jlist = begin_list;
	if (begin_list != NULL)
	{
		while ((jlist = jlist->next) != NULL)
			limit[1]++;
		while (limit[0] < limit[1])
		{
			n = limit[0]++;
			jlist = begin_list;
			while (n++ < limit[1])
				jlist = jlist->next;
			tmp = begin_list->data;
			begin_list->data = jlist->data;
			jlist->data = tmp;
			begin_list = begin_list->next;
			limit[1]--;
		}
	}
}
