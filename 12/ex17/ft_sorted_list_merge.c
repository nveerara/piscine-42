/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sorted_list_merge.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/09 22:22:51 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/10 10:46:29 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"
#include <stdlib.h>

void	ft_sorted_list_merge_lists(t_list *begin_list1, t_list *begin_list2,
		int (*cmp)())
{
	t_list	*pclist;
	t_list	*clist;
	t_list	*tmp;

	clist = begin_list1;
	while (begin_list2 != NULL)
	{
		while (clist != NULL && (*cmp)(clist->data, begin_list2->data) < 0)
		{
			pclist = clist;
			clist = clist->next;
		}
		pclist->next = begin_list2;
		tmp = begin_list2->next;
		begin_list2->next = clist;
		pclist = begin_list2;
		begin_list2 = tmp;
	}
}

void	ft_sorted_list_merge(t_list **begin_list1, t_list *begin_list2,
		int (*cmp)())
{
	t_list	*tmp;

	if (*begin_list1 != NULL)
	{
		while (begin_list2 != NULL &&
				(*cmp)((*begin_list1)->data, begin_list2->data) >= 0)
		{
			tmp = begin_list2->next;
			begin_list2->next = *begin_list1;
			*begin_list1 = begin_list2;
			begin_list2 = tmp;
		}
		ft_sorted_list_merge_lists(*begin_list1, begin_list2, cmp);
	}
	else
		*begin_list1 = begin_list2;
}
