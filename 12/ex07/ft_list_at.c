/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_at.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/09 18:49:41 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/09 19:22:01 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"
#include <stdlib.h>

t_list	*ft_list_at(t_list *begin_list, unsigned int nbr)
{
	unsigned int n;

	n = 0;
	while (n < nbr)
	{
		if (begin_list == NULL)
			return (NULL);
		begin_list = begin_list->next;
		n++;
	}
	return (begin_list);
}
