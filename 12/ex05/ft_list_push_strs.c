/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_push_strs.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/09 13:24:49 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/09 18:36:19 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"
#include <stdlib.h>

t_list	*ft_list_push_strs(int size, char **strs)
{
	int		n;
	t_list	*list;
	t_list	*tmp;

	n = 0;
	tmp = NULL;
	list = NULL;
	while (n < size)
	{
		list = ft_create_elem(strs[n]);
		list->next = tmp;
		tmp = list;
		n++;
	}
	return (list);
}
