/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sorted_list_insert.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/09 22:02:26 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/10 10:14:29 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"
#include <stdlib.h>

void	ft_sorted_list_insert(t_list **begin_list, void *data, int (*cmp)())
{
	t_list	*pclist;
	t_list	*clist;

	if (*begin_list != NULL)
	{
		clist = *begin_list;
		if ((*cmp)((*begin_list)->data, data) >= 0)
		{
			pclist = ft_create_elem(data);
			pclist->next = *begin_list;
			*begin_list = pclist;
		}
		else
		{
			while (clist != NULL && (*cmp)(clist->data, data) < 0)
			{
				pclist = clist;
				clist = clist->next;
			}
			pclist->next = ft_create_elem(data);
			pclist->next->next = clist;
		}
	}
	else
		*begin_list = ft_create_elem(data);
}
