/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rev_params.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/04 14:21:59 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/04 14:23:41 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		ft_strlen(char *str)
{
	int	cc;

	cc = 0;
	while (*(str + cc))
		cc++;
	return (cc);
}

void	ft_putstr(char *str)
{
	write(1, str, ft_strlen(str));
}

int		main(int ac, char **av)
{
	while (1 < ac)
	{
		ft_putstr(av[ac - 1]);
		ft_putstr("\n");
		ac--;
	}
}
