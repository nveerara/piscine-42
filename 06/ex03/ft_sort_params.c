/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_params.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/04 14:24:43 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/04 14:33:41 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		ft_strlen(char *str)
{
	int	cc;

	cc = 0;
	while (*(str + cc))
		cc++;
	return (cc);
}

void	ft_putstr(char *str)
{
	write(1, str, ft_strlen(str));
}

int		ft_strcmp(char *s1, char *s2)
{
	while (*s1 == *s2 && *s1)
	{
		s1++;
		s2++;
	}
	return (*s1 - *s2);
}

void	ft_sort_char_tab(char **tab, int size)
{
	int		i;
	int		j;
	char	*swap;

	i = 0;
	while (i < size)
	{
		j = i + 1;
		while (j < size)
		{
			if (0 < ft_strcmp(*(tab + i), *(tab + j)))
			{
				swap = *(tab + i);
				*(tab + i) = *(tab + j);
				*(tab + j) = swap;
			}
			j++;
		}
		i++;
	}
}

int		main(int ac, char **av)
{
	int n;

	n = 1;
	ft_sort_char_tab(av + 1, ac - 1);
	while (n < ac)
	{
		ft_putstr(av[n]);
		ft_putstr("\n");
		n++;
	}
}
