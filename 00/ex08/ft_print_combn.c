/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_combn.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/30 13:18:20 by nveerara          #+#    #+#             */
/*   Updated: 2020/06/30 17:17:19 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	print_comb(char *units, int n)
{
	write(1, units, n);
	write(1, ", ", 2);
}

void	ft_print_combn_rec(char *units, int n, int i)
{
	if (i == n - 1)
	{
		while (units[i] <= '9')
		{
			print_comb(units, n);
			units[i]++;
		}
	}
	else
	{
		while (units[i] <= '9')
		{
			units[i + 1] = units[i] + 1;
			ft_print_combn_rec(units, n, i + 1);
			units[i]++;
		}
	}
}

void	ft_print_combn(int n)
{
	char	units[10];
	int		i;

	units[0] = '0';
	i = n - 1;
	while (units[0] <= '9' - n)
	{
		units[1] = units[0] + 1;
		if (n == 1)
			print_comb(units, 1);
		else
			ft_print_combn_rec(units, n, 1);
		units[0]++;
	}
	units[i] = '9';
	while (i)
	{
		units[i - 1] = units[i] - 1;
		i--;
	}
	write(1, &units, n);
}
