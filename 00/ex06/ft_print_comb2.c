/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/30 10:41:14 by nveerara          #+#    #+#             */
/*   Updated: 2020/06/30 17:16:06 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putnbr_rec_fixed_size(int nb, int size)
{
	char	digit;

	if (nb != 0 || size > 0)
	{
		digit = '0' + nb % 10;
		size--;
		ft_putnbr_rec_fixed_size(nb / 10, size);
		write(1, &digit, 1);
	}
}

void	ft_putnbr_fixed_size(int nb, int size)
{
	if (nb < 0)
		write(1, "-", 1);
	ft_putnbr_rec_fixed_size(nb, size);
}

void	put_comb(int nb1, int nb2)
{
	ft_putnbr_fixed_size(nb1, 2);
	write(1, " ", 1);
	ft_putnbr_fixed_size(nb2, 2);
}

void	ft_print_comb2(void)
{
	int		numbers[2];

	numbers[0] = 0;
	while (numbers[0] <= 99 - 2)
	{
		numbers[1] = numbers[0] + 1;
		while (numbers[1] <= 99)
		{
			put_comb(numbers[0], numbers[1]);
			write(1, ", ", 2);
			numbers[1]++;
		}
		numbers[0]++;
	}
	numbers[1] = numbers[0] + 1;
	put_comb(numbers[0], numbers[1]);
}
