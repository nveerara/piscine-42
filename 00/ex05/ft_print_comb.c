/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/30 09:54:21 by nveerara          #+#    #+#             */
/*   Updated: 2020/06/30 17:12:00 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_print_comb(void)
{
	char	units[3];

	units[0] = '0';
	while (units[0] <= '9' - 3)
	{
		units[1] = units[0] + 1;
		while (units[1] <= '9')
		{
			units[2] = units[1] + 1;
			while (units[2] <= '9')
			{
				write(1, units, 3);
				write(1, ", ", 2);
				units[2]++;
			}
			units[1]++;
		}
		units[0]++;
	}
	units[1] = units[0] + 1;
	units[2] = units[1] + 1;
	write(1, units, 3);
}
