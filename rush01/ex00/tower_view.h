/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tower_view.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/11 10:33:06 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/12 19:05:32 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TOWER_VIEW_H
# define TOWER_VIEW_H

# include <unistd.h>
# include <stdio.h>

typedef	struct	s_tuples {
	int			tuple[255][4];
	int			size;
	int			selected;
}				t_tuples;

extern int		g_tmp_tuple[4];
extern int		g_views[4][4];
extern t_tuples	g_t_tuples[8];
extern void		(*g_print_func)(void);

int				check_input(int argc, char **argv);
int				check_tuples(void);
void			print_debug_view(void);
int				check_view_rl(void);
int				check_view_lr(void);
void			gen_tuples(void);
int				fill_check_tuples(void);
void			print_table(void);
void			fill_views(char *av);
int				ft_atoi(char *str);
void			ft_putstr(char *str);
void			ft_putnbr(int nb);
int				ft_strcmp(char *s1, char *s2);

#endif
