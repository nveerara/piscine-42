/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/30 13:03:28 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/01 11:52:05 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putnbr_rec(int nb)
{
	char	digit;

	if (nb != 0)
	{
		digit = '0' + (0 <= nb ? nb % 10 : nb % 10 * -1);
		ft_putnbr_rec(nb / 10);
		write(1, &digit, 1);
	}
}

void	ft_putnbr(int nb)
{
	if (nb == 0)
		write(1, "0", 1);
	else
	{
		if (nb < 0)
			write(1, "-", 1);
		ft_putnbr_rec(nb);
	}
}
