/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tuple_gen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/11 09:43:54 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/12 18:04:26 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tower_view.h"

static void	init_tmp_tuples(void)
{
	int i;

	i = 0;
	while (i < 4)
	{
		g_tmp_tuple[i] = 0;
		i++;
	}
}

static void	add_tuple(int orr, int n, int t)
{
	int i;

	i = 0;
	while (i < 4)
	{
		g_t_tuples[n * 2 + orr].tuple[t][i] = g_tmp_tuple[i];
		i++;
	}
}

static int	check_seq(void)
{
	int n;
	int i;
	int j;

	j = 0;
	while (j < 4)
	{
		i = 0;
		n = g_tmp_tuple[j];
		while (i < 4)
		{
			if (i != j)
			{
				if (n == g_tmp_tuple[i])
					return (0);
			}
			i++;
		}
		j++;
	}
	return (1);
}

static void	fill_tuple(int orr, int n)
{
	int depth;
	int	t;

	t = 0;
	while (g_tmp_tuple[0] != 4)
	{
		depth = 3;
		if (check_view_lr() == g_views[(orr == 0 ? 0 : 2)][n]
	&& check_view_rl() == g_views[(orr == 0 ? 1 : 3)][n]
	&& check_seq())
			add_tuple(orr, n, t++);
		g_tmp_tuple[depth]++;
		while (g_tmp_tuple[depth] == 4 && depth != 0)
		{
			g_tmp_tuple[depth] = 0;
			depth--;
			g_tmp_tuple[depth]++;
		}
	}
	g_t_tuples[n * 2 + orr].selected = 0;
	g_t_tuples[n * 2 + orr].size = t;
}

void		gen_tuples(void)
{
	int orr;
	int n;

	orr = 0;
	while (orr < 2)
	{
		n = 0;
		while (n < 4)
		{
			init_tmp_tuples();
			fill_tuple(orr, n);
			n++;
		}
		orr++;
	}
}
