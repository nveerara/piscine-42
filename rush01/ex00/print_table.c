/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_table.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/11 12:13:32 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/12 17:51:05 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tower_view.h"

static void	print_debug_top(void)
{
	int n;

	n = 0;
	ft_putstr("    ");
	while (n < 4)
	{
		ft_putnbr(g_views[0][n]);
		ft_putstr(" ");
		n++;
	}
	ft_putstr("\n");
	ft_putstr("    _ _ _ _\n");
}

static void	print_debug_bot(void)
{
	int n;

	n = 0;
	ft_putstr("    - - - -\n");
	ft_putstr("    ");
	while (n < 4)
	{
		ft_putnbr(g_views[1][n]);
		ft_putstr(" ");
		n++;
	}
	ft_putstr("\n");
}

void		print_debug_view(void)
{
	int		n;
	int		i;
	char	c[8];

	print_debug_top();
	n = 1;
	while (n < 8)
	{
		i = 0;
		while (i < 4)
		{
			c[i * 2] = g_t_tuples[n].tuple[g_t_tuples[n].selected][i] + '1';
			c[i * 2 + 1] = ' ';
			i++;
		}
		ft_putnbr(g_views[2][n / 2]);
		ft_putstr("  |");
		write(1, c, 7);
		ft_putstr("|  ");
		ft_putnbr(g_views[3][n / 2]);
		ft_putstr("\n");
		n += 2;
	}
	print_debug_bot();
}

void		print_table(void)
{
	int		n;
	int		i;
	char	c[8];

	n = 1;
	while (n < 8)
	{
		i = 0;
		while (i < 4)
		{
			c[i * 2] = g_t_tuples[n].tuple[g_t_tuples[n].selected][i] + '1';
			c[i * 2 + 1] = ' ';
			i++;
		}
		c[i * 2 + 1 - 2] = '\n';
		write(1, c, 8);
		n += 2;
	}
}
