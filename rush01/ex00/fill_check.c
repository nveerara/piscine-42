/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_check.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/11 11:00:33 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/12 16:20:25 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tower_view.h"

static int	check_valid(int depth)
{
	int n;
	int c;
	int i;

	while (depth < 8)
	{
		if (depth % 2)
			n = 0;
		else
			n = 1;
		i = 0;
		c = depth / 2;
		while (g_t_tuples[depth].tuple[g_t_tuples[depth].selected][i]
		== g_t_tuples[n].tuple[g_t_tuples[n].selected][c]
		&& n <= depth)
		{
			i++;
			n += 2;
		}
		if (n < depth)
			return (depth);
		depth++;
	}
	return (depth);
}

int			fill_check_tuples(void)
{
	int depth;
	int tmpdepth;

	depth = 0;
	while (g_t_tuples[0].selected < g_t_tuples[0].size)
	{
		if ((depth = check_valid(depth)) == 8)
		{
			(*g_print_func)();
			return (0);
		}
		tmpdepth = depth + 1;
		g_t_tuples[depth].selected++;
		while (tmpdepth < 8)
			g_t_tuples[tmpdepth++].selected = 0;
		while (g_t_tuples[depth].selected == g_t_tuples[depth].size
				&& 0 != depth)
		{
			g_t_tuples[depth].selected = 0;
			depth--;
			g_t_tuples[depth].selected++;
		}
	}
	return (1);
}
