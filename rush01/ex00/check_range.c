/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_range.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/11 10:47:06 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/11 13:52:19 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tower_view.h"

int	check_view_lr(void)
{
	int view;
	int height;
	int i;

	view = 1;
	i = 1;
	height = g_tmp_tuple[0];
	while (i < 4)
	{
		if (height < g_tmp_tuple[i])
		{
			view++;
			height = g_tmp_tuple[i];
		}
		i++;
	}
	return (view);
}

int	check_view_rl(void)
{
	int view;
	int height;
	int i;

	view = 1;
	i = 2;
	height = g_tmp_tuple[3];
	while (0 <= i)
	{
		if (height < g_tmp_tuple[i])
		{
			view++;
			height = g_tmp_tuple[i];
		}
		i--;
	}
	return (view);
}
