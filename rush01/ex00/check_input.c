/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_input.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sobouatt <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/12 12:26:20 by sobouatt          #+#    #+#             */
/*   Updated: 2020/07/12 16:18:28 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tower_view.h"

int		check_input(int argc, char **argv)
{
	int i;

	i = 0;
	if (argc != 2 && argc != 3)
		return (0);
	while (argv[1][i] != '\0')
	{
		if (argv[1][i] < '1' || argv[1][i] > '4')
			return (0);
		i++;
		if (argv[1][i] != ' ' && argv[1][i] != '\0')
			return (0);
		if (argv[1][i])
			i++;
	}
	if (i != 31)
		return (0);
	if (argc == 3 && ft_strcmp("debug", argv[2]) == 0)
		g_print_func = &print_debug_view;
	else if (argc == 3)
		return (0);
	return (1);
}

int		check_tuples(void)
{
	int	i;

	i = 0;
	while (i < 8)
	{
		if (g_t_tuples[i].size == 0)
			return (1);
		i++;
	}
	return (0);
}
