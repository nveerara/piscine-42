/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/11 09:45:41 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/12 18:19:54 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tower_view.h"

#include <stdio.h>

int			g_views[4][4];
int			g_tmp_tuple[4];
t_tuples	g_t_tuples[8];
void		(*g_print_func)(void) = &print_table;

int				main(int ac, char **av)
{
	if (check_input(ac, av))
	{
		fill_views(av[1]);
		gen_tuples();
		if (check_tuples() || fill_check_tuples())
		{
			write(STDOUT_FILENO, "Error\n", 6);
			return (1);
		}
	}
	else
		write(STDOUT_FILENO, "Error\n", 6);
	return (0);
}
