/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_views.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/11 09:47:07 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/12 11:56:58 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tower_view.h"

char	*inc_to_nb(char *nbr)
{
	while (!('0' <= *nbr && *nbr <= '9'))
		nbr++;
	return (nbr);
}

void	fill_views(char *arg)
{
	int side;
	int n;

	side = 0;
	while (side < 4)
	{
		n = 0;
		while (n < 4)
		{
			g_views[side][n] = ft_atoi(arg);
			arg += 2;
			n++;
		}
		side++;
	}
}
