/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strs_to_tab.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/05 22:53:24 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/09 18:17:27 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_stock_str.h"

int					ft_strlen(char *str)
{
	int	cc;

	cc = 0;
	while (*(str + cc))
		cc++;
	return (cc);
}

char				*ft_strcpy(char *dest, char *src)
{
	char *bdest;

	bdest = dest;
	while (*src)
		*(dest++) = *(src++);
	*dest = '\0';
	return (bdest);
}

char				*ft_strdup(char *src)
{
	char	*copy;
	int		n;

	n = 0;
	if ((copy = (char *)malloc(sizeof(char) * (ft_strlen(src) + 1))) == NULL)
		return (NULL);
	ft_strcpy(copy, src);
	return (copy);
}

struct s_stock_str	*ft_strs_to_tab(int ac, char **av)
{
	int					n;
	struct s_stock_str	*tab;

	n = 0;
	if ((tab = (struct s_stock_str *)malloc(sizeof(struct s_stock_str)
					* (ac + 1)))
			== NULL)
		return (NULL);
	while (n < ac)
	{
		tab[n].size = ft_strlen(av[n]);
		tab[n].str = av[n];
		tab[n].copy = ft_strdup(av[n]);
		n++;
	}
	tab[n].str = 0;
	return (tab);
}
