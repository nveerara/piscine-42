/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_advanced_sort_string_tab.c                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/07 00:28:10 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/12 10:49:08 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

void	ft_advanced_sort_string_tab(char **tab, int (*cmp)(char *, char *))
{
	int		i;
	char	*tmp;
	int		swapped;

	i = 0;
	swapped = 1;
	if (tab[i] != NULL)
	{
		while (tab[i + 1] != NULL)
		{
			if (0 < (*cmp)(tab[i], tab[i + 1]))
			{
				tmp = tab[i];
				tab[i] = tab[i + 1];
				tab[i + 1] = tmp;
				swapped = 1;
			}
			i++;
			if (tab[i + 1] == NULL && swapped)
			{
				i = 0;
				swapped = 0;
			}
		}
	}
}
