/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   doop.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/06 20:38:33 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/06 21:35:22 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DOOP_H
# define DOOP_H

# define DIV_ZERO_MSG "Stop : division by zero"
# define MOD_ZERO_MSG "Stop : modulo by zero"

# define OPERATORS "+-*/%"

int		ft_atoi(char *str);
void	ft_putnbr(int nb);
void	ft_putstr(char *str);
void	add(int x, int y);
void	sub(int x, int y);
void	mult(int x, int y);
void	div(int x, int y);
void	mod(int x, int y);
void	err(int x, int y);
int		checkop(char *str);

#endif
