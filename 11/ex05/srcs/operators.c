/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   operators.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/06 19:52:03 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/06 20:45:35 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "doop.h"

void	add(int x, int y)
{
	ft_putnbr(x + y);
}

void	sub(int x, int y)
{
	ft_putnbr(x - y);
}

void	mult(int x, int y)
{
	ft_putnbr(x * y);
}

void	div(int x, int y)
{
	if (y)
		ft_putnbr(x / y);
	else
		ft_putstr(DIV_ZERO_MSG);
}

void	mod(int x, int y)
{
	if (y)
		ft_putnbr(x % y);
	else
		ft_putstr(MOD_ZERO_MSG);
}
