/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/06 20:35:04 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/06 21:46:38 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "doop.h"

void	(*g_operator[])(int, int) = {&err,
	&add,
	&sub,
	&mult,
	&div,
	&mod};

int		main(int ac, char **av)
{
	if (ac == 4)
	{
		g_operator[checkop(av[2])](ft_atoi(av[1]), ft_atoi(av[3]));
		ft_putstr("\n");
	}
	return (0);
}
