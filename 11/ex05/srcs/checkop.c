/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checkop.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/06 20:46:05 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/07 00:40:32 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "doop.h"

int		checkop(char *str)
{
	char	*operators;
	int		n;

	operators = OPERATORS;
	n = 0;
	while (operators[n])
	{
		if (*str == operators[n] && *(str + 1) == '\0')
			return (n + 1);
		n++;
	}
	return (0);
}
