/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/02 12:06:23 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/05 17:51:36 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		is_whitespace(char c)
{
	return (c == ' ' || c == '\r' || c == '\f'
			|| c == '\v' || c == '\n' || c == '\t');
}

int		is_digit(char c)
{
	return ('0' <= c && c <= '9');
}

int		ft_atoi(char *str)
{
	int		neg;
	int		atoi;

	neg = 1;
	atoi = 0;
	while (is_whitespace(*str))
		str++;
	while (*str == '-' || *str == '+')
	{
		if (*str == '-')
			neg *= -1;
		str++;
	}
	while (is_digit(*str))
	{
		atoi *= 10;
		atoi += *str - 0x30;
		str++;
	}
	atoi *= neg;
	return (atoi);
}
