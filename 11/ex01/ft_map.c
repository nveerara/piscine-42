/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_map.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/06 19:01:36 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/07 00:43:33 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		*ft_map(int *tab, int length, int (*f)(int))
{
	int	*ntab;
	int	n;

	if ((ntab = (int *)malloc(sizeof(int) * length)) == NULL)
		return (NULL);
	n = 0;
	while (n < length)
	{
		*(ntab + n) = f(*(tab + n));
		n++;
	}
	return (ntab);
}
