/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_sort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/06 19:11:07 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/07 00:43:55 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_is_sort(int *tab, int length, int (*f)(int, int))
{
	int n;
	int sorted;

	n = 0;
	sorted = 1;
	while (n < length - 1)
	{
		if (f(*(tab + n), *(tab + n + 1)) < 0)
			sorted = 0;
		n++;
	}
	if (!sorted)
	{
		n = 0;
		sorted = 1;
		while (n < length - 1)
		{
			if (f(*(tab + n), *(tab + n + 1)) > 0)
				sorted = 0;
			n++;
		}
	}
	return (sorted);
}
