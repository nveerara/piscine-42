/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_next_prime.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/03 15:06:17 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/04 21:32:49 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_sqrt(int nb)
{
	int	root;
	int old;
	int test;

	old = -1;
	test = -2;
	root = nb / 2;
	if (nb <= 0)
		return (0);
	else if (nb == 1)
		return (1);
	else if (nb == 2)
		return (0);
	while (old != root && test != root)
	{
		test = old;
		old = root;
		root = (root + nb / root) / 2;
	}
	return (root);
}

int		ft_is_prime(int nb)
{
	int limit;
	int i;

	if (nb <= 1)
		return (0);
	if (nb == 2)
		return (1);
	limit = ft_sqrt(nb) + 1;
	if (nb % 2 == 0)
		return (0);
	i = 3;
	while (i <= limit)
	{
		if (nb % i == 0)
			return (0);
		i += 2;
	}
	return (1);
}

int		ft_find_next_prime(int nb)
{
	if (nb < 2)
		return (2);
	else if (ft_is_prime(nb))
		return (nb);
	if (nb % 2)
		nb += 2;
	else
		nb += 1;
	while (!ft_is_prime(nb))
		nb += 2;
	return (nb);
}
