/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ten_queens_puzzle.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/03 16:09:37 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/04 14:05:32 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		position_safe(char *queen, int index)
{
	int check;

	check = index - 1;
	while (0 <= check)
	{
		if (queen[check] == queen[index])
			return (0);
		if (queen[check] + (index - check) == queen[index])
			return (0);
		if (queen[check] - (index - check) == queen[index])
			return (0);
		check--;
	}
	return (1);
}

void	print_sol(char *queen)
{
	int n;

	n = 0;
	while (n < 10)
	{
		*(queen + n) += 0x30;
		write(1, queen + n, 1);
		*(queen + n) -= 0x30;
		n++;
	}
	write(1, "\n", 1);
}

void	ft_ten_queens_rec(char *queen, int index, int *res)
{
	int pos;

	if (index == 10)
	{
		(*res)++;
		print_sol(queen);
	}
	else
	{
		pos = 0;
		while (pos < 10)
		{
			queen[index] = pos;
			if (position_safe(queen, index))
				ft_ten_queens_rec(queen, index + 1, res);
			pos++;
		}
	}
}

int		ft_ten_queens_puzzle(void)
{
	char	queen[10];
	int		res;

	res = 0;
	ft_ten_queens_rec(queen, 0, &res);
	return (res);
}
