/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sqrt.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/03 13:56:41 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/04 16:57:00 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_sqrt(int nb)
{
	int	root;
	int old;
	int test;

	old = -1;
	test = -2;
	root = nb / 2;
	if (nb <= 0)
		return (0);
	else if (nb == 1)
		return (1);
	else if (nb == 2)
		return (0);
	while (old != root && test != root)
	{
		test = old;
		old = root;
		root = (root + nb / root) / 2;
	}
	if (nb == root * root)
		return (root);
	else
		return (0);
}
