/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_power.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/03 12:39:29 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/04 13:43:26 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_iterative_power(int nb, int power)
{
	int ret;

	if (power < 0)
		return (0);
	else if (power == 0)
		return (1);
	ret = 1;
	while (power)
	{
		ret *= nb;
		power--;
	}
	return (ret);
}
