/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/04 17:58:45 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/06 17:01:21 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_strlen(char *str)
{
	int	cc;

	cc = 0;
	while (*(str + cc))
		cc++;
	return (cc);
}

int		strcpy_sep(char *dest, char *src)
{
	int		i;

	i = 0;
	while (*src)
	{
		*(dest++) = *(src++);
		i++;
	}
	return (i);
}

char	*ft_strjoin(int size, char **strs, char *sep)
{
	int		n;
	int		sjlen;
	char	*strj;
	int		i;

	n = 0;
	sjlen = 0;
	while (n < size)
		sjlen += ft_strlen(strs[n++]) + ft_strlen(sep);
	if ((strj = (char *)malloc(sizeof(char) * sjlen + 1)) == NULL)
		return (NULL);
	n = 0;
	sjlen = 0;
	while (n < size)
	{
		i = 0;
		while (strs[n][i])
			strj[sjlen++] = strs[n][i++];
		sjlen += strcpy_sep(strj + sjlen, sep);
		n++;
	}
	if (n)
		sjlen -= ft_strlen(sep);
	strj[sjlen] = '\0';
	return (strj);
}
