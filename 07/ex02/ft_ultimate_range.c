/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultimate_range.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/04 15:37:39 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/05 15:09:45 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_ultimate_range(int **range, int min, int max)
{
	int		n;

	if (max <= min)
	{
		*range = NULL;
		return (0);
	}
	if ((*range = (int *)malloc(sizeof(int) * (max - min))) == NULL)
		return (-1);
	n = 0;
	while (min + n < max)
	{
		*((*range) + n) = min + n;
		n++;
	}
	return (max - min);
}
