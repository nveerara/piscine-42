/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/06 23:04:03 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/08 10:26:21 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char	*ft_strndup(char *src, int n)
{
	char	*copy;
	int		a;

	if ((copy = (char *)malloc(sizeof(char) * n + 1)) == NULL)
		return (NULL);
	a = 0;
	while (a < n)
	{
		copy[a] = src[a];
		a++;
	}
	copy[a] = '\0';
	return (copy);
}

int		ft_strchar(char c, char *str)
{
	while (*str)
	{
		if (c == *str)
			return (1);
		str++;
	}
	return (0);
}

int		word_count(char *str, char *charset)
{
	int i;
	int	n;

	i = 0;
	n = 1;
	while (str[i])
	{
		if (ft_strchar(str[i], charset))
		{
			if (i)
			{
				n++;
				str += i;
				i = 0;
			}
			else
				str++;
		}
		else
			i++;
	}
	if (i)
		n++;
	return (n);
}

void	fill_split(char *str, char *charset, char **splits)
{
	int i;
	int n;

	i = 0;
	n = 0;
	while (str[i])
	{
		if (ft_strchar(str[i], charset))
		{
			if (i)
			{
				splits[n++] = ft_strndup(str, i);
				str += i;
				i = 0;
			}
			else
				str++;
		}
		else
			i++;
	}
	if (i)
		splits[n++] = ft_strndup(str, i);
	splits[n] = 0;
}

char	**ft_split(char *str, char *charset)
{
	char	**splits;

	if ((splits = (char **)malloc((sizeof(char *)
						* word_count(str, charset)))) == NULL)
		return (NULL);
	fill_split(str, charset, splits);
	return (splits);
}
