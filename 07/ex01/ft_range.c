/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/04 14:44:34 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/05 14:14:00 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		*ft_range(int min, int max)
{
	int		*range;
	int		n;

	if (max <= min)
		return (NULL);
	if ((range = (int *)malloc(sizeof(int) * (max - min))) == NULL)
		return (NULL);
	n = 0;
	while (min + n < max)
	{
		*(range + n) = min + n;
		n++;
	}
	return (range);
}
