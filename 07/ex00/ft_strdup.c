/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/04 14:40:02 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/06 23:38:17 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_strlen(char *str)
{
	int	cc;

	cc = 0;
	while (*(str + cc))
		cc++;
	return (cc);
}

char	*ft_strcpy(char *dest, char *src)
{
	char *bdest;

	bdest = dest;
	while (*src)
		*(dest++) = *(src++);
	*dest = '\0';
	return (bdest);
}

char	*ft_strdup(char *src)
{
	char	*copy;

	if ((copy = (char *)malloc(sizeof(char) * (ft_strlen(src) + 1))) == NULL)
		return (NULL);
	ft_strcpy(copy, src);
	return (copy);
}
