/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_base.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/04 20:24:50 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/06 14:13:08 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_atoi_base(char *str, char *base);
int		check_base(char *base);
int		ft_strlen(char *str);

int		size_nb_base(int nb, char *base)
{
	int len;

	len = 0;
	if (nb == 0)
		return (1);
	if (nb < 0)
		len++;
	while (nb)
	{
		nb /= ft_strlen(base);
		len++;
	}
	return (len);
}

void	nb_str_to_base(int nb, char *base, char *str)
{
	int neg;

	neg = 0;
	if (nb < 0)
		neg = 1;
	if (nb == 0)
		*(--str) = *base;
	while (nb)
	{
		*(--str) = (nb < 0 ? *(base - nb % ft_strlen(base))
					: *(base + nb % ft_strlen(base)));
		nb /= ft_strlen(base);
	}
	if (neg)
		*(--str) = '-';
}

char	*ft_convert_base(char *nbr, char *base_from, char *base_to)
{
	int		nb;
	char	*conv;

	if (check_base(base_from) || check_base(base_to))
		return (NULL);
	nb = ft_atoi_base(nbr, base_from);
	if ((conv = (char *)malloc(sizeof(char)
					* size_nb_base(nb, base_to) + 1)) == NULL)
		return (NULL);
	*(conv + size_nb_base(nb, base_to)) = '\0';
	nb_str_to_base(nb, base_to, conv + size_nb_base(nb, base_to));
	return (conv);
}
