/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_base2.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/04 21:38:37 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/05 17:26:59 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_strlen(char *str)
{
	int	cc;

	cc = 0;
	while (*(str + cc))
		cc++;
	return (cc);
}

int		is_base(char c, char *base)
{
	while (*base)
	{
		if (c == *base)
			return (1);
		base++;
	}
	return (0);
}

int		check_base(char *base)
{
	int i;
	int j;

	i = 0;
	while (*(base + i))
		i++;
	if (i == 0 || i == 1)
		return (1);
	i = 0;
	while (*(base + i))
	{
		j = 1;
		if (*(base + i) == '-' || *(base + i) == '+' || *(base + i) == ' '
	|| *(base + i) == '\t' || *(base + i) == '\n' || *(base + i) == '\f'
	|| *(base + i) == '\v' || *(base + i) == '\r')
			return (1);
		while (*(base + j + i))
		{
			if (*(base + i + j) == *(base + i))
				return (1);
			j++;
		}
		i++;
	}
	return (0);
}

int		char_to_base(char c, char *base)
{
	int i;

	i = 0;
	while (base[i])
	{
		if (c == base[i])
			return (i);
		i++;
	}
	return (0);
}

int		ft_atoi_base(char *str, char *base)
{
	int		neg;
	int		atoi;

	if (check_base(base))
		return (0);
	neg = 1;
	atoi = 0;
	while (*str == ' ' || *str == '\r' || *str == '\f'
			|| *str == '\v' || *str == '\t' || *str == '\n')
		str++;
	while (*str == '-' || *str == '+')
	{
		if (*str == '-')
			neg *= -1;
		str++;
	}
	while (is_base(*str, base))
	{
		atoi *= ft_strlen(base);
		atoi += char_to_base(*str, base);
		str++;
	}
	atoi *= neg;
	return (atoi);
}
